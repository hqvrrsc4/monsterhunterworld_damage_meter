﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Diagnostics;
using System.Threading;

namespace mhw_dps_wpf {

    public partial class MainWindow : Window {

        public MainWindow() {
            this.Topmost = true;

            this.AllowsTransparency = true;
            this.WindowStyle = WindowStyle.None;
            this.Background = Brushes.Transparent;

            find_game_proc();
            InitializeComponent();

        }

        static void assert(bool flag, string reason = "", bool show_reason = true) {
            if (!flag) {
                if (show_reason)
                    MessageBox.Show("assertion failed: " + reason);
                Application.Current.Shutdown();
                return;
            }
        }

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();



        Process game;
        private void find_game_proc() {
            var procs = Process.GetProcesses().Where(x => x.ProcessName == ("MonsterHunterWorld"));
            assert(procs.Count() == 1, "frm_main_load: #proc not 1.");
            game = procs.FirstOrDefault();
        }

        string[] player_names = new string[4];
        int[] player_damages = new int[4] { 4, 5, 6, 7 };
        int my_seat_id = -5;

        private void update_tick(object sender, EventArgs e) {
            if (game.HasExited)
                Application.Current.Shutdown();
            if (init_finished) {
                var new_dmg_numbers = mhw.get_team_dmg(game);
                var new_player_names = mhw.get_team_player_names(game);
                int new_seat_id = mhw.get_player_seat_id(game);
                if (new_dmg_numbers.Sum() != 0 && new_seat_id >= 0 && new_player_names[0] != "") {
                    player_damages = new_dmg_numbers;
                    player_names = new_player_names;
                    my_seat_id = new_seat_id;
                    update_info(false);
                } else if (my_seat_id != -5) {
                    update_info(true);
                }

            }
        }

        Rectangle[] damage_bar_rects = new Rectangle[4];
        TextBlock[] player_name_tbs = new TextBlock[4];
        TextBlock[] player_dmg_tbs = new TextBlock[4];

        void update_info(bool quest_end) {
            if (!init_finished) return;
            int total_dmg = player_damages.Sum();
            if (quest_end) {
                for (int i = 0; i < 4; i++) {
                    player_name_tbs[i].Text = player_names[i];
                    player_dmg_tbs[i].Text = player_names[i] == "" ? "" : player_damages[i].ToString() + " (" +
                        ((float)player_damages[i] / total_dmg * 100).ToString("0.0") + "%)";
                }
                return;
            }

            for (int i = 0; i < 4; i++) {
                player_name_tbs[i].Text = player_names[i];
                player_dmg_tbs[i].Text = player_names[i] == "" ? "" : "" + " " +
                    ((float)player_damages[i] / total_dmg * 100).ToString("0.0") + "%";
            }

            if (total_dmg == 0) {
                for (int i = 0; i < 4; i++) {
                    damage_bar_rects[i].Width = 0.0;
                }
            } else {
                for (int i = 0; i < 4; i++) {
                    damage_bar_rects[i].Width = (double)player_damages[i] / player_damages.Max() * front_canvas.ActualWidth;
                    if (i == my_seat_id) {
                        damage_bar_rects[i].StrokeThickness = 1;
                    } else {
                        damage_bar_rects[i].StrokeThickness = 0;
                    }
                }
            }
        }


        void update_layout() {
            if (!init_finished) return;
            double bar_height = (front_canvas.ActualHeight - 8) * .2f;
            double bar_offset = (front_canvas.ActualHeight - bar_height) / 3 - 2;
            for (int i = 0; i < 4; i++) {
                damage_bar_rects[i].Height = bar_height;
                Canvas.SetTop(damage_bar_rects[i], i * bar_offset + 3);
                Canvas.SetTop(player_name_tbs[i], i * bar_offset + 3 + 0.5 * bar_height - 12);

                Canvas.SetTop(player_dmg_tbs[i], i * bar_offset + 3 + 0.5 * bar_height - 14);
                Canvas.SetLeft(player_dmg_tbs[i], front_canvas.ActualWidth - player_dmg_tbs[i].Width);
            }

            int total_dmg = player_damages.Sum();
            if (total_dmg == 0) {
                for (int i = 0; i < 4; i++) {
                    damage_bar_rects[i].Width = 0.0;
                }
            } else {
                for (int i = 0; i < 4; i++) {
                    damage_bar_rects[i].Width = (double)player_damages[i] / player_damages.Max() * front_canvas.ActualWidth;
                    if (i == my_seat_id) {
                        damage_bar_rects[i].StrokeThickness = 1;
                    } else {
                        damage_bar_rects[i].StrokeThickness = 0;
                    }
                }
            }

        }

        static Color[] player_colors = new Color[] {
            Color.FromRgb(225,65,55),
            Color.FromRgb(53, 136, 227),
            Color.FromRgb(196, 172, 44),
            Color.FromRgb(42, 208, 55)
        };

        bool init_finished = false;
        void init_canvas() {
            init_finished = true;
            double bar_height = front_canvas.ActualHeight * .2f - 1.75;
            double bar_offset = (front_canvas.ActualHeight - bar_height) / 3;
            for (int i = 0; i < 4; i++) {
                damage_bar_rects[i] = new Rectangle();
                damage_bar_rects[i].Stroke = new SolidColorBrush(Colors.White);
                damage_bar_rects[i].StrokeThickness = 0;
                damage_bar_rects[i].Fill = new SolidColorBrush(player_colors[i]);
                damage_bar_rects[i].Fill.Opacity = 0.65;
                damage_bar_rects[i].Width = 200;
                damage_bar_rects[i].Height = bar_height;
                Canvas.SetTop(damage_bar_rects[i], i * bar_offset);
                front_canvas.Children.Add(damage_bar_rects[i]);

                player_name_tbs[i] = new TextBlock();



                player_name_tbs[i].FontSize = 16;
                player_name_tbs[i].Width = 220;
                player_name_tbs[i].Height = 40;
                player_name_tbs[i].FontWeight = FontWeights.Bold;
                player_name_tbs[i].Foreground = new SolidColorBrush(Colors.White);
                player_name_tbs[i].Effect = new System.Windows.Media.Effects.DropShadowEffect(

                   ) { ShadowDepth = 0, Color = Colors.Black, BlurRadius = 4, Opacity = 1 };
                Canvas.SetTop(player_name_tbs[i], i * bar_offset + 0.5 * bar_height - 14);
                Canvas.SetLeft(player_name_tbs[i], 3);
                front_canvas.Children.Add(player_name_tbs[i]);

                player_dmg_tbs[i] = new TextBlock();
                player_dmg_tbs[i].TextAlignment = TextAlignment.Right;
                player_dmg_tbs[i].Text = (i * 4000).ToString() + " (125.4%)";
                player_dmg_tbs[i].Effect = new System.Windows.Media.Effects.DropShadowEffect(

                    ) { ShadowDepth = 0, Color = Colors.Black, BlurRadius = 4, Opacity = 1 };
                //player_dmg_tbs[i].FontFamily = new FontFamily("Century Gothic");
                player_dmg_tbs[i].FontWeight = FontWeights.Bold;
                player_dmg_tbs[i].FontSize = 16;
                player_dmg_tbs[i].Foreground = new SolidColorBrush(Colors.White);
                player_dmg_tbs[i].Width = 175;
                player_dmg_tbs[i].Height = 40;
                Canvas.SetTop(player_dmg_tbs[i], i * bar_offset + 0.5 * bar_height - 14);
                Canvas.SetLeft(player_dmg_tbs[i], front_canvas.ActualWidth - player_dmg_tbs[i].Width - 3);
                front_canvas.Children.Add(player_dmg_tbs[i]);
            }

            player_name_tbs[0].Text = "拖统计条：移动窗口";
            player_name_tbs[1].Text = "滚轮：放大缩小窗口";
            player_name_tbs[2].Text = "右键+滚轮：拉长统计条";
            player_name_tbs[3].Text = "退出：右键任务栏-关闭";
            update_layout();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            init_canvas();

            dispatcherTimer.Tick += update_tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 150);
            dispatcherTimer.Start();
            this.ShowInTaskbar = false;
            this.ShowInTaskbar = true;

        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e) {
            update_layout();
        }

        static double time() {
            return (DateTime.UtcNow - DateTime.MinValue).TotalSeconds;
        }
        double last_activated = time();
        private void Window_Activated(object sender, EventArgs e) {
            if (time() - last_activated > 5) {

            } else {

            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e) {
            try {
                if (e.LeftButton == MouseButtonState.Pressed) {
                    this.DragMove();
                }


            } catch (Exception ex) { }

        }

        private void Window_MouseWheel(object sender, MouseWheelEventArgs e) {
            if (e.RightButton == MouseButtonState.Pressed) {
                var new_width = this.Width + (float)e.Delta * 0.1f;
                if (new_width > this.MinWidth) {
                    this.Width = new_width;
                }
            } else if (e.LeftButton == MouseButtonState.Pressed) {
                var new_height = this.Height + (float)e.Delta * 0.1f;
                if (new_height > this.MinHeight) {
                    this.Height = new_height;
                }
            } else {
                var new_width = this.Width + (float)e.Delta * 0.07f;
                if (new_width > this.MinWidth) {
                    this.Width = new_width;
                }
                var new_height = this.Height + (float)e.Delta * 0.03f;
                if (new_height > this.MinHeight) {
                    this.Height = new_height;
                }
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

public static class mhw {

    [DllImport("kernel32.dll")]
    static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, Int32 dwSize, ref int lpNumberOfBytesRead);

    static bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer) {
        int dummy = 0;
        return ReadProcessMemory(hProcess, lpBaseAddress, lpBuffer, lpBuffer.Length, ref dummy);
    }

    static ulong read_ulong(IntPtr hProcess, IntPtr lpBaseAddress) {
        byte[] buffer = new byte[8];
        ReadProcessMemory(hProcess, lpBaseAddress, buffer);
        return BitConverter.ToUInt64(buffer, 0);
    }

    static uint read_uint(IntPtr hProcess, IntPtr lpBaseAddress) {
        byte[] buffer = new byte[4];
        ReadProcessMemory(hProcess, lpBaseAddress, buffer);
        return BitConverter.ToUInt32(buffer, 0);
    }

    static int dword_to_int(ref byte[] array) {
        return (array[0] + (array[1] << 8) + (array[2] << 16) + (array[3] << 24));
    }

    static ulong asm_func1(Process proc, ulong rcx, uint edx) {//proc 141747030
        uint r8d = read_uint(proc.Handle, (IntPtr)0x14396c95c);
        ulong rax = 0;
        ulong r9 = rcx;
        r8d &= edx;
        rax = r8d;
        rcx = rax * 0x58;
        rax = r9 + 0x48;
        rax += rcx;
        return rax;
    }

    public static int[] get_team_dmg(Process proc) {
        int[] result = new int[4] { 0, 0, 0, 0 };

        byte[] buffer = new byte[4];
        byte[] w_buffer = new byte[8];
        ReadProcessMemory(proc.Handle, (IntPtr)0x143b1dae8, w_buffer);
        ulong a1 = BitConverter.ToUInt64(w_buffer, 0);
        ulong rbx = a1 + 0x66d4;
        ulong rdi = a1 + 0x66b0;
        ReadProcessMemory(proc.Handle, (IntPtr)0x143b204b8, w_buffer);
        ulong rcx = BitConverter.ToUInt64(w_buffer, 0);
        for (int i = 0; i < 4; i++) {
            ReadProcessMemory(proc.Handle, (IntPtr)(rdi + 4 * (ulong)i), buffer);

            uint edx = read_uint(proc.Handle, (IntPtr)(rdi + 4 * (ulong)i));
            ulong rax = asm_func1(proc, rcx, edx);

            if (rax > 0) {
                ReadProcessMemory(proc.Handle, (IntPtr)(rax + 0x48), w_buffer);
                ulong stats_base_ptr = BitConverter.ToUInt64(w_buffer, 0);
                if (stats_base_ptr > 0) {
                    bool success = ReadProcessMemory(proc.Handle, (IntPtr)(stats_base_ptr + 0x48), buffer);
                    int dmg_num = dword_to_int(ref buffer);
                    if (success && dmg_num >= 0 && dmg_num <= 0xfffff)
                        result[i] = dmg_num;
                }
            }
        }
        return result;
    }

    public static int get_player_seat_id(Process proc) {
        uint ptr1 = read_uint(proc.Handle, (IntPtr)0x144813e90L);
        uint v2 = read_uint(proc.Handle, (IntPtr)(ptr1 + 600));
        int v5 = -1;
        if (v2 > 0x1000) {
            uint v3 = read_uint(proc.Handle, (IntPtr)(v2 + 16));
            if (v3 != 0)
                v5 = (int)read_uint(proc.Handle, (IntPtr)(v3 + 49132));
        }
        return v5;
    }

    public static string[] get_team_player_names(Process proc) {
        string[] result = new string[4];
        byte[] string_buffer = new byte[40];
        int loc1 = (int)read_uint(proc.Handle, (IntPtr)0x144813E90) + 0x54a45;
        for (int i = 0; i < 4; i++) {
            Array.Resize(ref string_buffer, 40);
            ReadProcessMemory(proc.Handle, (IntPtr)(loc1 + 33 * i), string_buffer);
            int last_ind = Array.FindIndex(string_buffer, x => x == 0);
            if (last_ind == 0 || last_ind == -1) {
                result[i] = "";
                continue;
            }
            Array.Resize(ref string_buffer, last_ind);
            result[i] = Encoding.UTF8.GetString(string_buffer);
        }
        return result;
    }
}

